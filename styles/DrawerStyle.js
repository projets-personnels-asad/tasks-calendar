const DrawerStyle = {
    drawerPosition: "left",
    contentOptions: {
      //activeTintColor: '#e91e63',
      //activeTintColor: '#5472AE',
      itemsContainerStyle: {
        marginVertical: 0,
      },
      iconContainerStyle: {
        opacity: 1
      }
    }
}

export default DrawerStyle;
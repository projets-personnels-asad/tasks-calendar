import React from 'react';
import { StyleSheet } from 'react-native';


const ButtonStyle = {
    primaryButton: {
      backgroundColor:  "#009587",
      margin: 5
    },
    secondaryButton: {
      backgroundColor:  "#0091f4",
      margin: 5
    },
    tertiaryButton:{
      backgroundColor: "#81c343",
      margin: 5
    },
    alertButton: {
      backgroundColor: "#aa2e25",
      margin: 5
    },
    warningButton:{
      backgroundColor: "#ff5122",
      margin: 5
    },
    lightButton: {
      backgroundColor: "#ffc000",
      margin: 5
    },
}

const Style = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
});

export {ButtonStyle, Style};
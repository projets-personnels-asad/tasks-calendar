import { AsyncStorage } from "react-native"
import { addNotification, removeNotification } from "@services/notifications/notifications"
import { tasksByAsc } from "@helpers/tasks"
import { merge } from 'lodash'

const initialState = {
  maxId: 0,
  data: []
}

const tasks = (state = initialState, action) => {
    let newState = merge({}, state)
    switch (action.type) {
      case 'LOAD_TASKS':
        newState = loadTasks(newState, action.tasks)
        return newState
      case 'ADD_TASK':
        newState = addTask(newState, action.task)
        return newState
      case 'EDIT_TASK':
        newState = editTask(newState, action.task)
        return newState
      case 'REMOVE_TASK':
        newState = removeTask(newState, action.task)
        return newState
      default:
        return state;
    }
}

function loadTasks(state, tasks){
  tasks = JSON.parse(tasks);
  if(tasks.length > 0){
    state.data = tasks;
    state.maxId = Math.max.apply(Math, state.data.map(function(task) { return task.id; }));
  }
  console.log("*** loading tasks ***")
  console.log(state)
  return state;
}

function addTask(state, task){
  task.id = ++state.maxId;
  state.data = [...state.data, task];
  tasksByAsc(state.data);
  AsyncStorage.setItem('TASKS', JSON.stringify(state.data));

  console.log("*** adding tasks ***")
  console.log(state)
  addNotification(task);
  return state;
}

function editTask(state, task){
  index = state.data.findIndex(t => t.id === task.id);
  state.data[index] = task;
  tasksByAsc(state.data);
  AsyncStorage.setItem('TASKS', JSON.stringify(state.data));

  console.log("*** editing tasks ***")
  console.log(state)
  removeNotification(task);
  addNotification(task);
  return state;
}

function removeTask(state, task){
  state.data = state.data.filter(t => t.id !== task.id);
  AsyncStorage.setItem('TASKS', JSON.stringify(state.data));

  console.log("*** removing tasks ***")
  console.log(state)
  removeNotification(task);
  return state;
}

export default tasks
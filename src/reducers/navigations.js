import { NavigationActions } from 'react-navigation';
import { DrawerNavigator } from '@navigators/DrawerNavigator';

const firstAction = DrawerNavigator.router.getActionForPathAndParams('Home');
const initialNavState = DrawerNavigator.router.getStateForAction(
  firstAction
);

const nav = (state = initialNavState, action) => {
    let nextState;
    switch (action.type) {
      case 'Home':
        nextState = DrawerNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'Home' }),
          state
        );
        break;
      case 'Planning':
        nextState = DrawerNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'Planning' }),
          state
        );
        break;
      case 'Notifications':
        nextState = DrawerNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'Notifications' }),
          state
        );
      break;
      case 'Settings':
        nextState = DrawerNavigator.router.getStateForAction(
          NavigationActions.navigate({ routeName: 'Settings' }),
          state
        );
      break;
      default:
        nextState = DrawerNavigator.router.getStateForAction(action, state);
        break;
    }
  
    return nextState || state;
}
  
export default nav
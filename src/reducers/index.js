import { combineReducers } from 'redux'
import nav from './navigations'
import tasks from './tasks'

export default combineReducers({
  nav,
  tasks
})
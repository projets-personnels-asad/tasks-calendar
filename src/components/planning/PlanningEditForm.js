import React from 'react';
import { Icon, FormLabel, FormInput, FormValidationMessage, Button } from 'react-native-elements';
import { StyleSheet, ScrollView, View, Picker, Alert } from 'react-native';
import DatePicker from 'react-native-datepicker'
import {Style, ButtonStyle} from "@styles/Style";
import {stringToDate} from "@helpers/date"

import { connect } from 'react-redux'
import { editTask } from '@actions/tasks'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
});

class PlanningEditForm extends React.Component {
  constructor (props) {
    super(props);

    let task = props.navigation.getParam('task', null);
    if(!task) props.navigation.goBack();

    this.state = {
      id: task.id,
      date: task.date,
      time: task.time,
      title: task.title,
      type: task.type
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      headerLeft: <Icon iconStyle={{padding:10}} color="#fff" name="arrow-back" onPress={() => navigation.goBack()}/>
    };
  };

  onEdit(){
    if(this.state.date !== "" && this.state.time !== "" && this.state.title !== "" && this.state.type !== ""){
      if(stringToDate(this.state).getTime() > Date.now()){
        this.props.editTask(this.state);
        this.props.navigation.goBack();
      }else{
        Alert.alert(
          "Erreur !",
          "La date doit être posterieur à la date du jour",
          [
            {text: 'OK'}
          ],
          { cancelable: false }
        )
      }
    }else{
      Alert.alert(
        "Erreur !",
        "Veuillez remplir tout les champs",
        [
          {text: 'OK'}
        ],
        { cancelable: false }
      )
    }
  }

  render () {
    return (
      <ScrollView>
        <View>
            <FormLabel>Titre</FormLabel>
            <FormInput value={this.state.title} onChangeText={(text) => this.setState({title: text})}/>
        </View>

        <Picker
          selectedValue={this.state.type}
          style={{ marginLeft: 15, marginRight: 15 }}
          onValueChange={(itemValue, itemIndex) => this.setState({type: itemValue})}>
          <Picker.Item label="Choisissez type" value="" />
          <Picker.Item label="Entretien" value="Entretien" />
          <Picker.Item label="Reunion" value="Réunion" />
          <Picker.Item label="Rendez-vous" value="Rendez-vous" />
          <Picker.Item label="Tâche personnelle" value="Tâche personnelle" />
        </Picker>

        <View style={{ marginTop: 20, marginBottom: 10, marginLeft: 15, flex: 1, flexDirection: 'row'}}>
          <Icon iconStyle={{marginRight:5}} color="#000" name="date-range"/>
          <DatePicker
            date={this.state.date}
            mode="date"
            format="DD/MM/YYYY"
            placeholder="Selectionner date"
            confirmBtnText="Confirmer"
            cancelBtnText="Annuler"
            showIcon={false}
            onDateChange={(date) => {this.setState({date: date})}}
            style={{width:250}}
          />
        </View>

        <View style={{ marginBottom: 20, marginLeft: 15, flex: 1, flexDirection: 'row'}}>
          <Icon iconStyle={{marginRight:5}} color="#000" name="alarm-add"/> 
          <DatePicker
            date={this.state.time}
            mode="time"
            placeholder="Selectionner heure"
            confirmBtnText="Confirmer"
            cancelBtnText="Annuler"
            showIcon={false}
            onDateChange={(time) => {this.setState({time: time})}}
            style={{width:250}}
          />
        </View>

         <Button large buttonStyle={ ButtonStyle.secondaryButton } title='Editer' onPress={() => this.onEdit()} />
         <Button large buttonStyle={ ButtonStyle.tertiaryButton }  title='Annuler' onPress={() => this.props.navigation.goBack()} />
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
  editTask: task => dispatch(editTask(task))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanningEditForm)
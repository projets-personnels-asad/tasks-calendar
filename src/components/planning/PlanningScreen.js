import React from 'react';
import { Button, Icon } from 'react-native-elements';
import { StyleSheet, Text, ScrollView, View } from 'react-native';
import {Calendar} from 'react-native-calendars';
import {Style, ButtonStyle} from "@styles/Style";
import PlanningList from "@containers/planning/PlanningList"
import {removeTask} from "@actions/tasks"
import { connect } from 'react-redux';

const styles = StyleSheet.create({
  /*container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },*/
});

class PlanningScreen extends React.Component {
  constructor (props) {
    super(props);

    this.state = {
      date: "",
      time: "",
      titre: "",
      type: ""
    }
  }

  static navigationOptions = ({ navigation, navigationOptions }) => {
    return {
      headerRight: <Icon iconStyle={{padding:10}} color="#fff" name="add" onPress={() => navigation.push("PlanningAddForm")}/>
    };
  };

  render () {
    return (
      <ScrollView style={styles.container}>
        <PlanningList {...this.props}></PlanningList>
        
        {/*
        <Calendar
          onDayPress={this.onDayPress}
          style={styles.calendar}
          hideExtraDays
          markedDates={{[this.state.date]: {selected: true, disableTouchEvent: true, selectedDotColor: 'orange'}}}
        />

        <Button large buttonStyle={ ButtonStyle.secondaryButton } rightIcon={{name: 'add'}} title='Nouveau' onPress={() => this.props.navigation.push("PlanningAddForm")} />
        */}
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => ({
  tasks: state.tasks.data
})

const mapDispatchToProps = (dispatch) => ({
  removeTask: task => dispatch(removeTask(task))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PlanningScreen)
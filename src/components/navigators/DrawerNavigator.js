import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { createStackNavigator, createDrawerNavigator } from 'react-navigation';

import HeaderStyle from '@styles/HeaderStyle';
import DrawerStyle from '@styles/DrawerStyle';
import ContentDrawer from './ContentDrawer';

import { connect } from 'react-redux';

import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
} from 'react-navigation-redux-helpers';

import HomeScreen from '@components/home/HomeScreen';
import PlanningScreen from '@components/planning/PlanningScreen';
import PlanningAddForm from '@components/planning/PlanningAddForm';
import PlanningEditForm from '@components/planning/PlanningEditForm';
import NotificationsScreen from '@components/notifications/NotificationsScreen';
import SettingsScreen from '@components/settings/SettingsScreen';


// Fonction création d'une page stack
function createStack(routeConfigs, title){
  const nav = createStackNavigator(
      routeConfigs,
      {
          navigationOptions: ({ navigation }) => ({
            title: title,
            headerTitle: title,
            headerTitleStyle: HeaderStyle.headerTitleStyle,
            headerStyle: HeaderStyle.headerStyle,
            headerTintColor: HeaderStyle.headerTintColor,
            headerLeft: <Icon iconStyle={{padding:10}} color="#fff" name="menu" onPress={() => navigation.openDrawer()}/>,
            headerRight: <View></View>
          }),
      }
  );

  return nav;
}

// Fonction création du drawer
function createDrawer(screens){
  const nav = createDrawerNavigator(
      screens,
      {
          drawerPosition: DrawerStyle.drawerPosition,
          contentComponent: ContentDrawer,
          contentOptions: {
              //activeTintColor: '#e91e63',
              //activeTintColor: '#5472AE',
              itemsContainerStyle: DrawerStyle.itemsContainerStyle,
              iconContainerStyle: DrawerStyle.iconContainerStyle
          },
          initialRouteName: 'Home'
      }
  );

  return nav;
}


// Initialisation

const HomeRoot = {
  Home: HomeScreen,
}

const HomeStackScreen = createStack(HomeRoot, "Accueil");

const PlanningRoot = {
  PlanningScreen: PlanningScreen,
  PlanningAddForm: PlanningAddForm,
  PlanningEditForm: PlanningEditForm
}

const PlanningStackScreen = createStack(PlanningRoot, "Planning");

const NotifsRoot = {
  Notifications: NotificationsScreen,
}

const NotifsStackScreen = createStack(NotifsRoot, "Notifications");

const SettingsRoot = {
  Settings: SettingsScreen,
}

const SettingsStackScreen = createStack(SettingsRoot, "Paramètres");


/*const HomeStackScreen = createStack({HomeStackScreen: {screen: HomeScreen}}, "Accueil");
const PlanningStackScreen = createStack({PlanningStackScreen: {screen: PlanningScreen}}, "Planning");
const NotifsStackScreen = createStack({NotifsStackScreen: {screen: NotificationsScreen}}, "Notifications");
const SettingsStackScreen = createStack({SettingsStackScreen: {screen: SettingsScreen}}, "Paramètres");*/

const screens = {
  Home : { 
    screen: HomeStackScreen,
    navigationOptions: {
      title: "Accueil",
      drawerLabel: "Accueil",
      drawerIcon: ({tintColor}) => (<Icon name="home" color={tintColor} />)
    }
  },
  Planning: { 
    screen: PlanningStackScreen,
    navigationOptions: {
      title: "Planning",
      drawerLabel: "Planning",
      drawerIcon: ({tintColor}) => (<Icon name="alarm" color={tintColor} />)
    }
  },
  Notifications : { 
    screen: NotifsStackScreen,
    navigationOptions: {
      title: "Notifications",
      drawerLabel: "Notifications",
      drawerIcon: ({tintColor}) => (<Icon name="notifications" color={tintColor} />)
    } 
  },
  Settings : { 
    screen: SettingsStackScreen,
    navigationOptions: {
      title: "Paramètres",
      drawerLabel: "Paramètres",
      drawerIcon: ({tintColor}) => (<Icon name="settings" color={tintColor} />)
    }
  }
};

const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
);

const DrawerNavigator = createDrawer(screens);

const AppWithNavigationState = reduxifyNavigator(DrawerNavigator, 'root');

const mapStateToProps = state => ({
  state: state.nav,
});


const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

export { DrawerNavigator, AppNavigator, middleware };
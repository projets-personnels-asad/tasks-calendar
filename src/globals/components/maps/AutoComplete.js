import React from 'react';
import { Icon, FormLabel, FormInput, Button } from 'react-native-elements';
import { StyleSheet, ScrollView, View, FlatList, Picker, Alert, TouchableOpacity, Text } from 'react-native';
import { searchPlaces } from "@services/maps/maps";

export default class AutoComplete extends React.Component {
  constructor (props) {
    super(props);
    
    this.state = {
      address: (props.addressData) ? props.addressData.address : "",
      addressData: (props.addressData) ? props.addressData : {},
      data: []
    }
  }

  async onChangeAddress(){
    let data = await searchPlaces(this.state.address);
    console.log(data);
    this.setState({data: data});
  }

  onSelectAddress(addressData){
    this.setState({
      address: addressData.address,
      addressData: addressData,
      data: [] 
    });
  }

  render () {
    return (
      <View>
        <FormLabel>Adresse</FormLabel>
        <FormInput value={this.state.address} onChangeText={(address) => { this.setState({address: address}); this.onChangeAddress(); } } />
        
        <View style={styles.container}>
          <FlatList
            data={this.state.data}
            renderItem={({item, index}) =>
              <TouchableOpacity
                key = {index}
                onPress = {() => this.onSelectAddress(item)}>
                  <Text style={styles.item}>{item.address}</Text>
              </TouchableOpacity>
            }
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
        
        <View>
            { /*
               this.state.data.map((item, index) => (
                  <TouchableOpacity
                     key = {index}
                     onPress = {() => this.onSelectAddress(item)}>
                     <View style={styles.container}>
                      <Text>
                          {item.address}
                      </Text>
                     </View>
                  </TouchableOpacity>
               ))*/
            }
         </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
  },
  item: {
    fontSize: 12,
    textAlign: "center"
  },
})
import PushNotification from "react-native-push-notification"
import { stringToDate, dateToSeconds } from "@helpers/date"

PushNotification.configure({
    onNotification: function(notification) {
        console.log( 'NOTIFICATION:', notification );
    },
})

export function addNotification(task){
    let date = stringToDate(task);
    let date_now = new Date();
    date_now.setSeconds(0,0);
    let time_rest = (date.getTime() - date_now.getTime())

    PushNotification.localNotificationSchedule({
        id: task.id + "",
        number: 0,
        userInfo: {
            id: task.id
        },
        message: "My Notification Message",
        date: new Date(date_now.getTime() + time_rest)
    });
}

export function removeNotification(task){
    PushNotification.cancelLocalNotifications({id: task.id + ""});
}   
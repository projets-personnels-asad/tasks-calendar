import {API_KEY} from "./config"

export async function searchPlaces(place= "", lat= "", lon= ""){
    const url = "https://api.tomtom.com/search/2/search/" + encodeURIComponent(place) + ".JSON?key=" + API_KEY + "&typeahead=true"

    return await fetch(url)
        .then((resp) => resp.json())
        .then((resp) => {
            try {
                let results = resp.results;
                let places = [];
                for (let val of results) {
                    places.push({
                      name: ('poi' in val) ? val.poi.name : val.address.freeformAddress,
                      address: val.address.freeformAddress,
                      lat: val.position.lat,
                      lon: val.position.lon,
                    });
                }
                return places;
              } catch (error) {
                return [];
              }
        })
        .catch((error) => {
            return [];
        });
}
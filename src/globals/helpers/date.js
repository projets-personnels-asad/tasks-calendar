
export function stringToDate(task){
    let date = task.date.split('/');
    let time = task.time.split(':');

    return new Date(date[2], date[1] - 1, date[0], time[0], time[1], 0, 0);
}
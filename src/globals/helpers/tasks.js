import { stringToDate } from "@helpers/date"

export function tasksByAsc(data){
      data.sort((a, b) => {
      let date_a = stringToDate(a);
      let date_b = stringToDate(b);
  
      return ((new Date() - date_b) - (new Date() - date_a))
    });
}

export function tasksByType(data, type){
  var dataByType = data.find(function(task){
    return task.type === type
  })

  return dataByType
}
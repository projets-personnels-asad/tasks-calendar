# tasks-calendar
Application mobile permettant d'organiser ses tâches (React Native/Redux)

1) npm install
2) npm start -- --reset-cache
3) react-native bundle --platform android --dev false --entry-file index.js --bundle-output android/app/src/main/assets/index.android.bundle --assets-dest android/app/src/main/res
4) react-native run-android
5) Go to Dev Settings > Debug server host & port for device. Type your network ip address like that XXX.XXX.X.XX:8081
